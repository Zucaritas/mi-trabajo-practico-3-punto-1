package Fabrica;
import java.util.ArrayList;

public class Empleado extends Persona{

    private ArrayList<Turno> listaTurnos;
    private float costoPorHora;

    public Empleado(String nombre, String apellido, float costoPorHora){
 
        super(nombre, apellido);
        this.costoPorHora = costoPorHora;
        this.listaTurnos = new ArrayList<>();

    }

    public void agregarTurno(Turno nuevoTurno){
        
        this.listaTurnos.add(nuevoTurno);

    }

    public ArrayList<Turno> getmostrarTurno(){

        return this.listaTurnos;
        
    }

    public float getCostoPorHora(){
        return costoPorHora;
    }

    public void setcostoPorHora(float costoPorHora){
        this.costoPorHora = costoPorHora;
    }

    public String getNombre(){
        return super.getNombre();
    }
    public String getApellido(){
        return super.getApellido();
    }
}
