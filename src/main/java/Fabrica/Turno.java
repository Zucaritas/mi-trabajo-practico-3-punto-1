package Fabrica;

public class Turno{
    private int dia;
    private int mes;
    private int anio;
    private int horaDeIngreso;
    private int horaDeEgreso;

    public int getdia(){
        return dia;
    }

    public int getmes(){
        return mes;
    }

    public int getanio(){
        return anio;
    }

    public int gethoraDeIngreso(){
        return horaDeIngreso; 
    }
    
    public int gethoraDeEgreso(){
        return horaDeEgreso;
    }

    public Turno(int dia, int mes, int anio, int horaDeIngreso, int horaDeEgreso){

        this.dia = dia;
        this.mes = mes;
        this.anio = anio;

        this.horaDeIngreso = horaDeIngreso;
        this.horaDeEgreso = horaDeEgreso;

    }

    public int getHorasTrabajadasPorTurno(){
        return (horaDeEgreso - horaDeIngreso);
    }

}
