package Fabrica;

public class Principal{

    public static void main(String[] args){
        Administracion fabrica = new Administracion();
        Empleado nuevoEmpleado = new Empleado("Tomas", "Zurita", 1650);
        double sueldo = fabrica.calcularSueldo(nuevoEmpleado);
        Turno nuevoTurno = new Turno(27, 9, 2020, 8, 18);

        nuevoEmpleado.agregarTurno(nuevoTurno);

        fabrica.mostrarInformacion(nuevoEmpleado);
        System.out.println(sueldo);

    }
}