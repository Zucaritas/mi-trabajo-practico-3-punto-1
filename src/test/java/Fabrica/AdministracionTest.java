package Fabrica;
import Fabrica.Administracion;
import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;

public class AdministracionTest{

    @Test
    public  void calcularSueldoEmpleado(){
        Administracion admin = new Administracion();
        Empleado nuevoEmpleado = new Empleado("Tomas", "Zurita", 100);
        Turno nuevoTurnoDePrueba = new Turno(10, 10, 2010, 10, 14);
        admin.calcularSueldo(nuevoEmpleado);
        nuevoEmpleado.agregarTurno(nuevoTurnoDePrueba);

        assertEquals(1200.00, admin.calcularSueldo(nuevoEmpleado), 0);
    }

}