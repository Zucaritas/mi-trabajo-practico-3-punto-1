package Fabrica;
import Fabrica.Administracion;
import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;

public class EmpleadoTest {
    
    @Test
    
    public void mostrarTurnoEmpleado(){
        ArrayList<Turno> listaDePrueba = new ArrayList<>() ;
        Turno nuevoTurnoDePrueba = new Turno(10, 10, 2010, 10, 14);
        Empleado nuevoEmpleado = new Empleado("Sujeto", "Prueba", 69);
        nuevoEmpleado.agregarTurno(nuevoTurnoDePrueba);

        listaDePrueba.add(nuevoTurnoDePrueba);

        assertEquals(listaDePrueba, nuevoEmpleado.getmostrarTurno());

    }

}
